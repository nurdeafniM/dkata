//methode reduce
let kota=['Jakarta','Bandung','Bogor']
console.log(kota.map(c=> c.length).filter(c=> c>6).length)
console.log(kota.map(c=> c.length).filter(c=> c>6))
console.log(kota.map(c=> c.length).filter(c=> c>6).reduce((a,b)=> a+b))

//export to text function in class execrise1.test.js
const city=['Bandung','Majalengka','Cirebon','Bogor']
exports.longs = city.map(el => el.length)
exports.longsgt6 = city.map(el => el.length).filter(el => el > 6).length